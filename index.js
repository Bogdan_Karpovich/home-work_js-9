//1
const features1 = document.getElementsByClassName("feature");
console.log("Features (getElementsByClassName):", features1);

const features2 = document.querySelectorAll(".feature");
console.log("Features (querySelectorAll):", features2);

Array.from(features1).forEach(feature => {
    feature.style.textAlign = "center";
});
//2
const headings = document.querySelectorAll("h2");
headings.forEach(heading => {
    heading.textContent = "Awesome feature";
});
//3
const featureTitles = document.querySelectorAll(".feature-title");
featureTitles.forEach(title => {
    title.textContent += "!";
});
